package me.khrystal.store.ui.fragment;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.lzy.okgo.model.Response;

import me.khrystal.store.R;
import me.khrystal.store.base.BaseListFragment;
import me.khrystal.store.base.RecyclerAdapter;
import me.khrystal.store.base.RecyclerViewHolder;
import me.khrystal.store.bean.App;
import me.khrystal.store.bean.CommonResp;
import me.khrystal.store.callback.JsonCallback;
import me.khrystal.store.callback.OnItemClickListener;
import me.khrystal.store.http.HttpRequest;
import me.khrystal.store.ui.activity.AppDetailActivity;
import me.khrystal.store.utils.StringUtil;
import me.khrystal.store.utils.log.MLog;

/**
 * usage: 热门游戏
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class HotGameFragment extends BaseListFragment<App> implements OnItemClickListener {

    private static final int LINE_OF_COUNT = 4;

    private static final String TAG = HotGameFragment.class.getSimpleName();

    @Override
    public void loadData(final int page) {
        HttpRequest.getInstance().getHotGames(getActivity())
                .execute(new JsonCallback<CommonResp>(CommonResp.class) {
                    @Override
                    public void onSuccess(Response<CommonResp> response) {
                        bindData(response.body(), page == 0);
                    }

                    @Override
                    public void onError(Response<CommonResp> response) {
                        super.onError(response);
                        hideRefreshView();
                        MLog.e(TAG, "请求失败");
                    }
                });
    }

    @Override
    protected void addDataToList(CommonResp resp, boolean isRefresh) {
        if (isRefresh)
            adapter.addRefreshData(resp.datas);
        else
            adapter.addMoreData(resp.datas);
    }

    @Override
    protected RecyclerAdapter<App> generateAdapter() {
        RecyclerAdapter<App> recyclerAdapter = new RecyclerAdapter<App>(getActivity(), null, false) {
            @Override
            public int getItemLayoutId(int viewType) {
                return R.layout.item_common;
            }

            @Override
            public void bindData(RecyclerViewHolder holder, int position, App item) {
                if (!StringUtil.isNullOrEmpty(item.icon))
                    Glide.with(getActivity()).load(item.icon).into(holder.getImageView(R.id.ivItem));
                if (!StringUtil.isNullOrEmpty(item.name))
                    holder.getTextView(R.id.tvItem).setText(item.name);
            }
        };
        recyclerAdapter.setOnItemClickListener(this);
        return recyclerAdapter;
    }

    @Override
    protected RecyclerView.LayoutManager generateLayoutManager() {
        return new GridLayoutManager(getActivity(), LINE_OF_COUNT);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), AppDetailActivity.class);
        intent.putExtra(AppDetailActivity.INK_APP_NAME, adapterData.get(position).name);
        startActivity(intent);
    }
}
