package me.khrystal.store.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.lzy.okserver.OkDownload;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.khrystal.store.R;
import me.khrystal.store.adapter.DownloadAppAdapter;
import me.khrystal.store.base.BaseActivity;

/**
 * usage: 下载管理界面
 * author: kHRYSTAL
 * create time: 17/7/16
 * update time:
 * email: 723526676@qq.com
 */

public class DownloadManagerActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnStartAll)
    Button btnStartAll;
    @Bind(R.id.btnPauseAll)
    Button btnPauseAll;
    @Bind(R.id.btnDelAll)
    Button btnDelAll;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    private OkDownload okDownload;
    private DownloadAppAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_manager);
        ButterKnife.bind(this);
        initDownload();
        initView();
    }

    private void initDownload() {
        okDownload = OkDownload.getInstance();
    }

    private void initView() {
        //region toolbar config
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("下载管理");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        //endregion

        adapter = new DownloadAppAdapter(this, null, false);
        adapter.updateData(DownloadAppAdapter.TYPE_ALL);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish(); // back button
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.unRegister();
    }

    @OnClick(R.id.btnStartAll)
    public void startAll(View view) {
        okDownload.startAll();
    }

    @OnClick(R.id.btnPauseAll)
    public void pauseAll(View view) {
        okDownload.pauseAll();
    }

    @OnClick(R.id.btnDelAll)
    public void deleteAll(View view) {
        okDownload.removeAll(true);
        adapter.updateData(DownloadAppAdapter.TYPE_ALL);
    }
}
