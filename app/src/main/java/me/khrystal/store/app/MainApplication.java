package me.khrystal.store.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.DBCookieStore;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.lzy.okserver.OkDownload;
import com.lzy.okserver.task.XExecutor;

import java.util.logging.Level;

import me.khrystal.store.BuildConfig;
import me.khrystal.store.utils.LocalPathUtil;
import me.khrystal.store.utils.log.MLog;
import okhttp3.OkHttpClient;

/**
 * usage: 主入口
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class MainApplication extends MultiDexApplication implements XExecutor.OnAllTaskEndListener {

    public static Context APP_CONTEXT = null;
    public static SharedPreferences APP_PREFERENCE;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        configBasic();
        configCrashHandle();
        configLog();
        configHttpClient();
        configDownload();
    }

    private void configDownload() {
        // 下载路径
        OkDownload.getInstance().setFolder(Environment.getExternalStorageDirectory().getAbsolutePath() + LocalPathUtil.DOWNLOAD_DIR);
        // 最大并发数
        OkDownload.getInstance().getThreadPool().setCorePoolSize(3);
        // 所有任务完成监听
        OkDownload.getInstance().addOnAllTaskEndListener(this);
    }

    private void configCrashHandle() {
        // TODO: 17/7/15
    }

    private void configLog() {
        MLog.init(BuildConfig.DEBUG);
    }

    /**
     * 配置基础信息
     */
    private void configBasic() {
        APP_CONTEXT = this;
        MainApplication.APP_PREFERENCE = this.getApplicationContext()
                .getSharedPreferences("kHRYSTALStore", Context.MODE_PRIVATE);
    }

    // 配置okGo 请求框架
    private void configHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");
        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);
        //log颜色级别，决定了log在控制台显示的颜色
        loggingInterceptor.setColorLevel(Level.INFO);
        builder.addInterceptor(loggingInterceptor);
        //使用数据库保持cookie，如果cookie不过期，则一直有效
        builder.cookieJar(new CookieJarImpl(new DBCookieStore(this)));

        OkGo.getInstance().init(this)                       //必须调用初始化
                .setOkHttpClient(builder.build())               //建议设置OkHttpClient，不设置将使用默认的
                .setCacheMode(CacheMode.FIRST_CACHE_THEN_REQUEST)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(3);                               //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0

    }

    @Override
    public void onAllTaskEnd() {
        MLog.e("Store", "All Files Downloaded");
    }

    @Override
    public void onTerminate() {
        OkDownload.getInstance().removeOnAllTaskEndListener(this);
        super.onTerminate();
    }
}
