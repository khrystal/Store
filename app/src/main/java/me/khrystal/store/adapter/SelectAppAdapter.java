package me.khrystal.store.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.CheckBox;

import com.bumptech.glide.Glide;
import com.lzy.okserver.OkDownload;

import java.util.ArrayList;
import java.util.List;

import me.khrystal.store.R;
import me.khrystal.store.base.RecyclerAdapter;
import me.khrystal.store.base.RecyclerViewHolder;
import me.khrystal.store.bean.App;
import me.khrystal.store.callback.SelectCountCallback;
import me.khrystal.store.http.HttpRequest;
import me.khrystal.store.utils.StringUtil;

/**
 * usage: 支持多选的RecyclerViewAdapter
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class SelectAppAdapter extends RecyclerAdapter<App> {

    private SparseBooleanArray mSelectedPositions = new SparseBooleanArray();
    private SelectCountCallback selectCountCallback;
    private boolean mIsSelectable = false;

    public SelectAppAdapter(Context context, List<App> data, boolean useAnimation, SelectCountCallback callback) {
        super(context, data, useAnimation);
        selectCountCallback = callback;
    }

    /**
     * 刷新列表调用 默认刷新后全部选中
     */
    public void updateDataSet(List<App> list) {
        mData.clear();
        mData.addAll(list);
        mSelectedPositions = new SparseBooleanArray();
        for (int i = 0; i < mData.size(); i++) {
            mSelectedPositions.put(i, true);
        }
        if (selectCountCallback != null) {
            selectCountCallback.onSelectCountChanged(mData.size());
        }
    }

    /**
     * 获取选中的结果
     */
    public ArrayList<App> getSelectedItem() {
        ArrayList<App> selectList = new ArrayList<>();
        for (int i = 0; i < mData.size(); i++) {
            if (isItemChecked(i)) {
                selectList.add(mData.get(i));
            }
        }
        return selectList;
    }

    //设置给定位置条目的选择状态
    private void setItemChecked(int position, boolean isChecked) {
        mSelectedPositions.put(position, isChecked);
    }

    //根据位置判断条目是否选中
    private boolean isItemChecked(int position) {
        return mSelectedPositions.get(position);
    }

    //根据位置判断条目是否可选
    private boolean isSelectable() {
        return mIsSelectable;
    }

    //设置给定位置条目的可选与否的状态
    private void setSelectable(boolean selectable) {
        mIsSelectable = selectable;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_selected;
    }

    @Override
    public void bindData(RecyclerViewHolder holder, final int position, App item) {
        // 判断是否在下载队列 如果已经在 勾选checkbox, itemView不可点击 显示已在队列
        if (OkDownload.getInstance().getTask(HttpRequest.getDownloadUrl(item.name)) != null) {
            holder.getTextView(R.id.tvDownloading).setVisibility(View.VISIBLE);
            setItemChecked(position, true);
            holder.getView(R.id.cbItem).setEnabled(false);
            holder.itemView.setEnabled(false);
        } else {
            holder.getTextView(R.id.tvDownloading).setVisibility(View.GONE);
            holder.itemView.setEnabled(true);
            holder.getView(R.id.cbItem).setEnabled(true);
            // 如果不在下载队列 不需要判断是否勾选 交给mSelectedPositions判断
        }


        if (!StringUtil.isNullOrEmpty(item.name))
            holder.getTextView(R.id.tvTitle).setText(item.name);
        else
            holder.getTextView(R.id.tvTitle).setText("应用名称为空");
        if (!StringUtil.isNullOrEmpty(item.description))
            holder.getTextView(R.id.tvDesc).setText(item.description);
        else
            holder.getTextView(R.id.tvDesc).setText("应用描述为空");

        if (!StringUtil.isNullOrEmpty(item.icon))
            Glide.with(mContext).load(item.icon).into(holder.getImageView(R.id.ivIcon));

        final CheckBox checkBox = (CheckBox) holder.getView(R.id.cbItem);
        checkBox.setChecked(isItemChecked(position));
        // 覆盖父类点击事件
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 反选
                if (isItemChecked(position)) {
                    setItemChecked(position, false);
                }  else {
                    setItemChecked(position, true);
                }
                notifyDataSetChanged();
                if (selectCountCallback != null)
                    selectCountCallback.onSelectCountChanged(getSelectedItem().size());
            }
        });
    }

}
