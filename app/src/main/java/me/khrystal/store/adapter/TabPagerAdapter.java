package me.khrystal.store.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import me.khrystal.store.ui.fragment.CommonAppFragment;
import me.khrystal.store.ui.fragment.HotGameFragment;
import me.khrystal.store.ui.fragment.QuickInstallFragment;

/**
 * usage: viewpager adapter
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class TabPagerAdapter extends FragmentPagerAdapter {

    String[] title = {"一键装机", "常用软件", "热门游戏"};
    QuickInstallFragment quickInstallFragment;
    CommonAppFragment commonAppFragment;
    HotGameFragment hotGameFragment;

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (quickInstallFragment == null)
                    quickInstallFragment = new QuickInstallFragment();
                return quickInstallFragment;
            case 1:
                if (commonAppFragment == null)
                    commonAppFragment = new CommonAppFragment();
                return commonAppFragment;
            case 2:
                if (hotGameFragment == null)
                    hotGameFragment = new HotGameFragment();
                return hotGameFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
