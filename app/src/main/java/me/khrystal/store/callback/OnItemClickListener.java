package me.khrystal.store.callback;

import android.view.View;
/**
 * usage: item 点击回调
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */
public interface OnItemClickListener {
    void onItemClick(View view, int position);
}