package me.khrystal.store.callback;

/**
 * usage: 选中数目回调
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public interface SelectCountCallback {
    public void onSelectCountChanged(int count);
}
