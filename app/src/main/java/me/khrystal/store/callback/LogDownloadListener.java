package me.khrystal.store.callback;

import com.lzy.okgo.model.Progress;
import com.lzy.okserver.download.DownloadListener;

import java.io.File;

import me.khrystal.store.utils.log.MLog;

/**
 * usage:
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class LogDownloadListener extends DownloadListener {

    private static final String TAG = LogDownloadListener.class.getSimpleName();

    public LogDownloadListener() {
        super(TAG);
    }

    @Override
    public void onStart(Progress progress) {
        MLog.e(TAG, "onStart:" + progress);
    }

    @Override
    public void onProgress(Progress progress) {
        MLog.e(TAG, "onProgress:" + progress);
    }

    @Override
    public void onError(Progress progress) {
        MLog.e(TAG, "onError:" + progress);
        progress.exception.printStackTrace();
    }

    @Override
    public void onFinish(File file, Progress progress) {
        MLog.e(TAG, "onFinish:" + progress);
    }

    @Override
    public void onRemove(Progress progress) {
        MLog.e(TAG, "onRemove:" + progress);
    }
}
