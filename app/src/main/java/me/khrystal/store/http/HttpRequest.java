package me.khrystal.store.http;

import android.content.Context;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.request.GetRequest;

import me.khrystal.store.bean.CommonResp;

/**
 * usage: 接口请求类 单例
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class HttpRequest {

    //region combine common url
    private static final String PROTOCOL = "http://";
    private static final String HOST = "205.209.156.62";
    private static final String VERSION = "/v1";
    private static final String DIR_API = "/api";
    private static final String DIR_APK = "/app/download";
    private static final String COMMON_URL = PROTOCOL + HOST + VERSION + DIR_API;
    private static final String COMMON_DOWNLOAD = PROTOCOL + HOST + VERSION + DIR_APK;
    public static final String APK_SUFFIX = ".apk";
    //endregion

    //region request Urls
    private static final String GET_QUICK_INSTALL = COMMON_URL + "/quick_install.php";
    private static final String GET_COMMON_APP = COMMON_URL + "/common_apps.php";
    private static final String GET_HOT_GAMES = COMMON_URL + "/hot_games.php";
    // need param "name = %s"
    private static final String GET_APP_DETAIL_BY_NAME = COMMON_URL + "/app_details.php";
    // need param "version = [currentVersion]"
    private static final String GET_UPDATE_INFO = COMMON_URL + "/update.php";

    private static final String DOWNLOAD_PATH = COMMON_DOWNLOAD + "/%s" + APK_SUFFIX ;
    //endregion

    private static final String CACHE_QUICK_INSTALL = "quick_install";
    private static final String CACHE_COMMON_APP = "common_apps";
    private static final String CACHE_HOT_GAME = "hot_games";
    private static final String CACHE_APP_DETAIL = "app_detail";


    private static Object syncObj = new Object();
    private static HttpRequest httpRequest;

    private HttpRequest() {
    }

    public static HttpRequest getInstance() {
        synchronized (syncObj) {
            if (httpRequest == null) {
                synchronized (syncObj) {
                    httpRequest = new HttpRequest();
                }
            }
        }
        return httpRequest;
    }

    /**
     * 获取一键装机应用数据
     */
    public GetRequest<CommonResp> getQuickInstall(Context context) {
        return OkGo.<CommonResp>get(GET_QUICK_INSTALL)
                .tag(context)
                .cacheKey(CACHE_QUICK_INSTALL);
    }

    /**
     * 获取常用软件数据
     */
    public GetRequest<CommonResp> getCommonApps(Context context) {
        return OkGo.<CommonResp>get(GET_COMMON_APP)
                .tag(context)
                .cacheKey(CACHE_COMMON_APP);
    }

    /**
     * 获取热门游戏数据
     */
    public GetRequest<CommonResp> getHotGames(Context context) {
        return OkGo.<CommonResp>get(GET_HOT_GAMES)
                .tag(context)
                .cacheKey(CACHE_HOT_GAME);
    }

    /**
     * 获取应用详情
     */
    public GetRequest<CommonResp> getAppDetail(Context context, String appName) {
        return OkGo.<CommonResp>get(GET_APP_DETAIL_BY_NAME)
                .tag(context)
                .cacheKey(CACHE_APP_DETAIL)
                .cacheMode(CacheMode.NO_CACHE)
                .params("name", appName);
    }

    /**
     * 获取更新信息
     */
    public GetRequest<CommonResp> getUpdateInfo(Context context, String versionName) {
        return OkGo.<CommonResp>get(GET_UPDATE_INFO)
                .tag(context)
                .cacheMode(CacheMode.NO_CACHE)
                .params("version", versionName);

    }

    /**
     * 获取apk下载地址
     */
    public static String getDownloadUrl(String apkName) {
        return String.format(DOWNLOAD_PATH, apkName);
    }


}
