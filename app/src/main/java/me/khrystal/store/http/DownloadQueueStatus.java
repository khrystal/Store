package me.khrystal.store.http;

/**
 * usage: 应用下载队列枚举
 * author: kHRYSTAL
 * create time: 17/7/16
 * update time:
 * email: 723526676@qq.com
 */

public enum DownloadQueueStatus {
    INQUEUE, NOTINQUEUE
}
