package me.khrystal.store.bean;

import com.google.gson.annotations.SerializedName;

/**
 * usage: 应用实体类
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class App extends OrmDto{
    // 应用名称
    @SerializedName("name")
    public String name;

    // 应用图标
    @SerializedName("icon")
    public String icon;

    // 应用描述
    @SerializedName("discribe")
    public String description;

    // 下载地址
    @SerializedName("address")
    public String address;

    // Store 的版本信息
    @SerializedName("version")
    public String version;

}
