package me.khrystal.store.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * usage: 通用服务器返回对象
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class CommonResp extends OrmDto{

    // 状态码 0 为正常 非0为异常
    @SerializedName("code")
    public int code;

    // 返回信息
    @SerializedName("success")
    public String message;

    // 消息类型
    @SerializedName("type")
    public String type;

    // 数据列表
    @SerializedName("datas")
    public List<App> datas;

    // 是否需要更新
    @SuppressWarnings("update")
    public boolean needUpdate;


}
