package me.khrystal.store.base;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import me.khrystal.store.ui.fragment.ProgressDialogFragment;

/**
 * usage: Activity 基类
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected ProgressDialogFragment progressDialogFragment;

    protected void showProgress() {
        showProgress(null, null);
    }

    protected void showProgress(int progressMessageId, int progressTitleId) {
        showProgress(getString(progressMessageId), getString(progressTitleId));
    }

    protected void showProgress(String progressMessage, String progressTitle) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        progressDialogFragment = ProgressDialogFragment.newInstance(progressTitle, progressMessage);
        progressDialogFragment.show(ft, "dialog");
    }

    protected void showProgress(String progressMessage) {
        showProgress(progressMessage, null);
    }

    protected void showProgress(int progressMessageId) {
        showProgress(getString(progressMessageId));
    }

    protected void dismissProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismiss();
            progressDialogFragment = null;
        }
    }
}
