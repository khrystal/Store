package me.khrystal.store.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.khrystal.store.R;
import me.khrystal.store.bean.CommonResp;
import me.khrystal.store.utils.log.MLog;

/**
 * usage:
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

@SuppressWarnings("ResourceType")
public abstract class BaseListFragment<T> extends BaseFragment {
    private static final String TAG = BaseListFragment.class.getSimpleName();

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.refreshView)
    SwipeRefreshLayout refreshView;
    @Bind(R.id.emptyView)
    RelativeLayout emptyView;

    protected RecyclerAdapter<T> adapter;
    protected RecyclerView.LayoutManager layoutManager;
    protected List<T> adapterData = new ArrayList<>();

    @Nullable
    @Override
    @CallSuper
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(layoutResId() == 0 ? R.layout.fragment_base_list : layoutResId(), container, false);
        return view;
    }

    @Override
    @CallSuper
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        configRecyclerView();
    }

    /**
     * 配置列表
     */
    protected void configRecyclerView() {
        layoutManager = generateLayoutManager();
        MLog.e(TAG, "recyclerView is null :" + (recyclerView == null));
        MLog.e(TAG, "layoutManager is null :" + (layoutManager == null));
        recyclerView.setLayoutManager(layoutManager);
        adapter = generateAdapter();
        adapter.setData(adapterData);
        recyclerView.setAdapter(adapter);

        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(0);
            }
        });
        refreshView.setRefreshing(true);
        loadData(0);
    }


    public int layoutResId() {
        return 0;
    }

    /**
     * @hide
     */
    @Override
    @CallSuper
    protected void bindData(CommonResp resp, boolean isRefresh) {
        refreshView.setRefreshing(false);
        if (resp != null) {
            if (resp.code == 0) {
                addDataToList(resp, isRefresh);
                if (adapter != null)
                    adapter.notifyDataSetChanged();
                if (resp.datas != null && !resp.datas.isEmpty()) {
                    hideEmptyView();
                } else {
                    showEmptyView();
                }
            } else {
                Toast.makeText(getActivity(), resp.message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public abstract void loadData(int page);

    protected abstract void addDataToList(CommonResp resp, boolean isRefresh);

    protected abstract RecyclerAdapter<T> generateAdapter();

    protected abstract RecyclerView.LayoutManager generateLayoutManager();

    protected void hideEmptyView() {
        if (emptyView != null) {
            emptyView.setVisibility(View.GONE);
        }
    }

    protected void showEmptyView() {
        if (emptyView != null)
            emptyView.setVisibility(View.VISIBLE);
    }

    protected void hideRefreshView() {
        refreshView.setRefreshing(false);
    }
}
