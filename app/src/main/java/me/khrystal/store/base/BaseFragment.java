package me.khrystal.store.base;


import android.support.v4.app.Fragment;

import me.khrystal.store.bean.CommonResp;

/**
 * usage: do nothing
 * author: kHRYSTAL
 * create time: 17/7/15
 * update time:
 * email: 723526676@qq.com
 */

public abstract class BaseFragment extends Fragment{

    protected abstract void bindData(CommonResp resp, boolean isRefresh);
}
